using AutoMapper;
using contact_api.Dtos;
using contact_api.Models;

namespace contact_api.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserDtoForReturn>();
            CreateMap<UserDtoForReturn, User>();

            CreateMap<User, UserDtoForRegister>();
            CreateMap<UserDtoForRegister, User>();

            CreateMap<User, UserDtoForLogin>();
            CreateMap<UserDtoForLogin, User>();

        }
    }
}