using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using contact_api.Data;
using contact_api.Dtos;
using contact_api.Models;
using contact_api.Services.Interfaces;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace contact_api.Services.Repos
{
    public class AuthRepo : IAuthRepo
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public AuthRepo(DataContext context, IMapper mapper, IConfiguration configuration)
        {
            this._configuration = configuration;
            this._mapper = mapper;
            this._context = context;
        }

        public async Task<(bool passwordValid, UserDtoForReturn userDtoForReturn, string token)> Login(UserDtoForLogin userDtoForLogin)
        {
            //Get User from Database
            var userFromDatabase = await _context.User.SingleOrDefaultAsync(x => x.Username == userDtoForLogin.Username);
            //no user in database
            if (userFromDatabase == null)
            {
                return (false, null, string.Empty);
            }

            //VerifyPassword
            if (!VerifyPassword(userFromDatabase.Password, userDtoForLogin.Password))
            {
                return (false, null, string.Empty);
            }

            //Generate Token
            UserDtoForReturn result = _mapper.Map<UserDtoForReturn>(userFromDatabase);

            return (true,result,BuildToken(userFromDatabase));
        }

        public async Task<(bool isSuccess, UserDtoForReturn userDtoForReturn, string errorText)> Register(UserDtoForRegister userDtoForRegister)
        {
            var isSuccess = true;
            var errorText = "";

            //map dto to user
            User user = _mapper.Map<User>(userDtoForRegister);
            //hash password
            user.Password = CreatePasswordHash(user.Password);
            //add to database
            _context.User.Add(user);
            await _context.SaveChangesAsync();

            //map user to dto for return
            var userToReturn = _mapper.Map<UserDtoForReturn>(user);

            return (isSuccess, userToReturn, errorText);
        }

        private string CreatePasswordHash(string password)
        {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA512,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            return $"{Convert.ToBase64String(salt)}.{hashed}";
        }

        public bool VerifyPassword(string hash, string password)
        {
            var parts = hash.Split('.', 2);

            if (parts.Length != 2)
            {
                return false;
            }

            var salt = Convert.FromBase64String(parts[0]);
            var passwordHash = parts[1];

            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA512,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return passwordHash == hashed;
        }

        private string BuildToken(User user)
        {
            try
            {
                // key is case-sensitive
                var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                new Claim("UserId", user.UserId.ToString()),
                new Claim("username", user.Username),
                new Claim("position", ""),
                new Claim(ClaimTypes.Role, "")
            };

                var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["Jwt:ExpireDay"]));
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: _configuration["Jwt:Issuer"],
                    audience: _configuration["Jwt:Audience"],
                    claims: claims,
                    expires: expires,
                    signingCredentials: creds
                );
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            catch (System.Exception ex)
            {
                var a = ex.Message;
                throw;
            }


        }
    }
}