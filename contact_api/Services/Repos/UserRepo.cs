using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using contact_api.Data;
using contact_api.Dtos;
using contact_api.Models;
using contact_api.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace contact_api.Services.Repos
{

    public class UserRepo : IUserRepo
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UserRepo(DataContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public async Task<UserDtoForReturn> GetUser(int id)
        {
            var result = await _context.User.SingleOrDefaultAsync(x => x.UserId == id);
            var resultToReturn = _mapper.Map<UserDtoForReturn>(result);
            return resultToReturn;
        }

        public async Task<IEnumerable<UserDtoForReturn>> GetUsers()
        {
            var result = await _context.User.ToListAsync();
            var resultToReturn = _mapper.Map<IEnumerable<UserDtoForReturn>>(result);
            return resultToReturn;
        }

    }
}