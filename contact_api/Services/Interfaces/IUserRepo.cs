using System.Collections.Generic;
using System.Threading.Tasks;
using contact_api.Dtos;

namespace contact_api.Services.Interfaces
{
    public interface IUserRepo
    {
    Task<IEnumerable<UserDtoForReturn>> GetUsers(); 

    Task<UserDtoForReturn> GetUser(int id);
    }
}