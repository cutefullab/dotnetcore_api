using System.Threading.Tasks;
using contact_api.Dtos;

namespace contact_api.Services.Interfaces
{
    public interface IAuthRepo
    {
        Task<(bool isSuccess, UserDtoForReturn userDtoForReturn, string errorText)> Register(UserDtoForRegister userDtoForRegister);
        Task<(bool passwordValid, UserDtoForReturn userDtoForReturn, string token)> Login(UserDtoForLogin userDtoForLogin);
    }
}