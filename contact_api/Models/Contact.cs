﻿using System;
using System.Collections.Generic;

namespace contact_api.Models
{
    public partial class Contact
    {
        public int ContactId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
