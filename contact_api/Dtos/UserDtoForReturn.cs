using System;

namespace contact_api.Dtos
{
    public class UserDtoForReturn
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Position { get; set; }
        public DateTime? Created { get; set; }
    }
}