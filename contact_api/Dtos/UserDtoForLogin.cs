namespace contact_api.Dtos
{
    public class UserDtoForLogin
    {
       
        public string Username { get; set; }
        public string Password { get; set; } 
    }
}