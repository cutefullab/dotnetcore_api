using System;
using System.Linq;
using System.Threading.Tasks;
using contact_api.Dtos;
using contact_api.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace contact_api.Controller
{

    [Route("api")]
    public class AuthController : ControllerBase
    {

        ILogger<AuthController> _logger;
        private readonly IAuthRepo _authRepo;

        public AuthController(ILogger<AuthController> logger, IAuthRepo authRepo)
        {
            this._authRepo = authRepo;
            _logger = logger;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserDtoForRegister userDtoForRegister)
        {
            try
            {

                var result = await _authRepo.Register(userDtoForRegister);
                return Ok(new { result.isSuccess, result.userDtoForReturn, result.errorText });
            }
            catch (Exception e)
            {
                _logger.LogError("Error at register :" + e.Message);
                return BadRequest();
            }
        }
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserDtoForLogin userDtoForLogin)
        {
            try
            {
                var result = await _authRepo.Login(userDtoForLogin);
                return Ok(new { result.passwordValid, result.userDtoForReturn, result.token });
            }
            catch (Exception e)
            {
                _logger.LogError("Error at register :" + e.Message);
                return BadRequest();
            }
        }
    }
}